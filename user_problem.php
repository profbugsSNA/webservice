<?php
require_once 'DB_Functions.php';
header('Content-Type: application/json');
//header('Content-Type: bitmap');
$response = array();

$response['success'] = false;

$db = new DB_Functions();

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (isset($_GET['user_id'])) {
        $user_id = $_GET['user_id'];
        if (!empty($user_id)) {
            $response = $db->getUserProblems($user_id);
            echo json_encode(array("result" => $response));

        } else {
            $response['message'] = "empty fields";
            echo json_encode($response);
        }
    } else {
        $response['message'] = "No user id";
        echo json_encode($response);
    }


}
?>