<?php
require_once 'DB_Functions.php';
header('Content-Type: application/json');
//header('Content-Type: bitmap');
$response = array();

$response['success'] = false;

$db = new DB_Functions();


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST['title'], $_POST['problem'], $_POST['category'], $_POST['user'], $_POST['place'])) {
        $title = $_POST['title'];
        $problem = $_POST['problem'];
        $category = $_POST['category'];
        $user = $_POST['user'];
        $place = $_POST['place'];
        $encoded_string=null;
        $image_name=null;
        if( isset($_POST["encoded_string"],$_POST["image_name"])) {
            $encoded_string = $_POST["encoded_string"];
            $image_name = $_POST["image_name"];
        }

        if (!empty($title) && !empty($problem) && !empty($category) && !empty($user) && !empty($place)) {
            try {
                $id = $db->addProblem($title, $problem, $category, $user, $place);
                if ($id) {
                    //credit to youtube tutorial https://www.youtube.com/watch?v=3tEiiUOLemA
                    if ($encoded_string&&$image_name) {
                        $decoded_string = base64_decode($encoded_string);
                        $path = 'images/'.$image_name;
                        $file = fopen($path, "wb");
                        $is_written = fwrite($file, $decoded_string);
                        fclose($file);
                        if ($is_written > 0) {
                            $response['written']=true;
                            $is_inserted = $db->addPicture($path, $id);
                            if ($is_inserted) {
                                $response['success'] = true;
                                $response['inserted']=true;
                            } else {
                                $response['success'] = false;
                            }

                        }else{
                            $response['success'] = false;
                        }
                    } else {
                        $response['success'] = true;
                    }
                }
            } catch (PDOException $ex) {
                die(json_encode($ex));
            }
            echo json_encode($response);

        } else {
            $response['message'] = "empty fields";
            echo json_encode($response);
        }
    }
    else {
        $response['message'] = "Not set headers";
        echo json_encode($response);
    }
}

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    $response = $db->getProblems();

    echo json_encode(array("result" => $response));


}

?>
