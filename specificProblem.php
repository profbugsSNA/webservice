<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12/25/16
 * Time: 7:58 PM
 */
require_once 'DB_Functions.php';
header('Content-Type: application/json');
//header('Content-Type: bitmap');
$response = array();

$response['success'] = false;

$db = new DB_Functions();

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (isset($_GET['problem_id'])) {
        $problem_id = $_GET['problem_id'];
        if (!empty($problem_id)) {
            $response = $db->getProblem($problem_id);
            echo json_encode(array("result" => $response));

        } else {
            $response['message'] = "empty fields";
            echo json_encode($response);
        }
    } else {
        $response['message'] = "No user id";
        echo json_encode($response);
    }


}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST['title'], $_POST['problem'], $_POST['category'], $_POST['id'], $_POST['place'])) {
        $title = $_POST['title'];
        $problem = $_POST['problem'];
        $category = $_POST['category'];
        $id = $_POST['id'];
        $place = $_POST['place'];

        if (!empty($title) && !empty($problem) && !empty($category) && !empty($id) && !empty($place)) {
            try {
                $result = $db->updateProblem($id, $title, $problem, $category, $place);
                if ($result === true) $response['success'] = true;
                else $response['message']="bad things happn";
            } catch (PDOException $ex) {
                die(json_encode($ex));
            }
            echo json_encode($response);

        } else {
            $response['message'] = "empty fields";
            echo json_encode($response);
        }
    } else {
        $response['message'] = "Not set headers";
        echo json_encode($response);
    }
}

?>
