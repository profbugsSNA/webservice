<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 11/22/16
 * Time: 8:04 PM
 */
require_once 'connection.php';

class DB_Functions
{
    private $conn;

    function __construct()
    {
        $db = new DB_Connection ();
        $this->conn = $db->get_connection();
    }

    public function userExists($email)
    {
        $query = "Select email from users where email ='$email'";
        $result = $this->conn->prepare($query);
        $result->execute();
        $email = $result->fetch();
        $result->closeCursor();
        if ($email) {
            return true;
        } else {
            return false;
        }

    }

    public function registerUser($firstname, $lastname, $email, $password)
    {
        $query1 = " Insert into users(firstname, lastname, email, password) values ('$firstname', '$lastname', '$email', '$password')";


        $res = $this->conn->prepare($query1);
        $is_inserted = $res->execute();
        $res->closeCursor();
        if ($is_inserted == 1) {
            $query = "Select id from users where email ='$email'";
            $result = $this->conn->prepare($query);
            $result->execute();
            $id = $result->fetch();
            $result->closeCursor();
            return $id=$id['id'];
        } else {
            return false;
        }

    }

    public function loginUser($email)
    {
        $query = "SELECT * FROM users " .
            " WHERE email = '$email'";
        $res = $this->conn->prepare($query);
        $res->execute();
        $result = $res->fetch();
        $res->closeCursor();

        if ($result) {
            return $result;
        } else {
            return false;
        }


    }

    public function addProblem($title, $problem, $category, $user, $place)
    {
        $query = " Insert into problems(title, statement, category, user, place) values ('$title', '$problem', '$category', '$user', '$place')";
        $res = $this->conn->prepare($query);
        $is_inserted = $res->execute();
        $id=$this->conn->lastInsertId();
        $res->closeCursor();
        if ($is_inserted == 1) {

            return $id;
        }
        return false;


    }

    public function updateProblem($problemId, $title, $statement, $category, $place){
        $query = "Update problems set title='$title', statement='$statement', category='$category', place='$place' where id='$problemId'";
        $res = $this->conn->prepare($query);
        $is_updated = $res->execute();
        $res->closeCursor();
        if ($is_updated== true) {

            return true;
        }
        return false;
    }

    public function addPicture($path, $id){
        $query= "INSERT INTO pictures(path, problem) VALUES('$path', '$id') ";
        $res = $this->conn->prepare($query);
        $is_inserted = $res->execute();
        $res->closeCursor();
        if ($is_inserted == 1) {
            return true;
        }
        return false;
    }

    public function getProblems()
    {
        $query = "SELECT p.title, p.statement, p.createdAt ,p.place,p.solved, i.path FROM problems p LEFT JOIN pictures i on p.id=i.problem where visibility=1 order by createdAt desc limit 20";
        $res = $this->conn->prepare($query);
        $res->execute();
        $problems = array();
        while ($row = $res->fetch()) {
            array_push($problems, array(
                    "title" => $row['title'],
                    "statement" => $row['statement'],
                    "createdAt" => $row['createdAt'],
                    "place" => $row['place'],
                    "path" => $row['path'],
                    "solved" => $row['solved'],
                )
            );
        }
        $res->closeCursor();
        if ($problems) {
            return $problems;
        }
    }
	
	public function getUserProblems($user_id)
    {
        $query = "SELECT p.id, p.title, p.statement, p.place, p.createdAt, p.solved, c.name FROM problems p left join categories c on p.category=c.id where visibility=1 and user='$user_id' order by createdAt desc";
        $res = $this->conn->prepare($query);
        $res->execute();
        $problems = array();
        while ($row = $res->fetch()) {
            array_push($problems, array(
                    "id" => $row['id'],
                    "title" => $row['title'],
                    "statement" => $row['statement'],
                    "category" => $row['name'],
                    "place" => $row['place'],
                    "solved" => $row['solved'],
                    "createdAt" => $row['createdAt']
                )
            );
        }
        echo json_encode(array("result" => $problems));


    }

    public function getCategories()
    {
        $query = "SELECT * FROM categories";
        $res = $this->conn->prepare($query);
        $res->execute();
        $categories = array();
        while ($row = $res->fetch()) {

            array_push($categories, array(
                    "id" => $row['id'],
                    "name" => $row['name']
                )
            );
        }
        $res->closeCursor();
        return $categories;

    }

    public function getProblem($problem_id){
        $query = "SELECT p.title, p.statement, p.place, p.createdAt, p.solved, c.name, c.id FROM problems p left join categories c on p.category=c.id where visibility=1 and p.id='$problem_id' order by createdAt desc";
        $res = $this->conn->prepare($query);
        $res->execute();
        $problems = array();
        while ($row = $res->fetch()) {
            array_push($problems, array(
                    "categoryId" => $row['id'],
                    "title" => $row['title'],
                    "statement" => $row['statement'],
                    "category" => $row['name'],
                    "place" => $row['place'],
                    "solved" => $row['solved'],
                    "createdAt" => $row['createdAt']
                )
            );
        }
        echo json_encode(array("result" => $problems));

    }

    public function  search(){

    }

}


?>
