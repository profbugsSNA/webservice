<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 11/10/16
 * Time: 2:04 AM
 */
require_once 'DB_Functions.php';
header('Content-Type: application/json');

$response=array();
$response['success']=false;
$response['message']="";
$db = new DB_Functions();

if( isset($_POST['firstName']) && isset($_POST['lastName'])&& isset($_POST['email'])&& isset($_POST['password']) ) {
    $firstname=$_POST['firstName'];
    $lastname=$_POST['lastName'];
    $email = $_POST['email'];
    $password = $_POST['password'];


   try{
      if($db->userExists($email)){
          $response['message']="Email already exists";
      }else {
             $id=$db->registerUser($firstname, $lastname, $email, $password);
              if($id) {
                  $response['id'] = $id;
                  $response['success'] = true;
              }

      }
  }catch(PDOException $ex){
      die(json_encode($ex));
  }

}
    echo json_encode($response);

?>
