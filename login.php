<?php
require_once 'DB_Functions.php';
header('Content-Type: application/json');
$response = array();
$response["success"] = false;
$db = new DB_Functions();
if (isset($_POST['email'])) {
    $email = $_POST['email'];


    try {
        $result = $db->loginUser($email);


        if ($result) {
            $response["success"] = true;
            $response['password'] = $result['password'];
            $response['firstName'] = $result['firstname'];
            $response['lastName'] = $result['lastname'];
            $response['id'] = $result['id'];
        }
    } catch (PDOException $ex) {
        die(json_encode($ex));
    }


}
echo json_encode($response);
?>

